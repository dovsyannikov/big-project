package com.company;

import com.company.model.*;
import com.company.model.Currency;

import java.util.*;

public class Generator {
    public static List<Organization> generate() {

        ///////////////////////////BANK 1

        Map<String, Map<String, Float>> currenciesBank = new HashMap<>();

        currenciesBank.put("USD", new HashMap<>(2, 1.1f));
        currenciesBank.get("USD").put("buy", 27.5f);
        currenciesBank.get("USD").put("sell", 27.5f);

        currenciesBank.put("EUR", new HashMap<>(2, 1.1f));

        currenciesBank.get("EUR").put("buy", 30.5f);
        currenciesBank.get("EUR").put("sell", 30.9f);

        currenciesBank.put("RUB", new HashMap<>(2, 1.1f));

        currenciesBank.get("RUB").put("buy", 0.45f);
        currenciesBank.get("RUB").put("sell", 0.4f);

        ///////////////////////////BANK 2

        Map<String, Map<String, Float>> currenciesBank2 = new HashMap<>();

        currenciesBank2.put("USD", new HashMap<>(2, 1.1f));
        currenciesBank2.get("USD").put("buy", 28.5f);
        currenciesBank2.get("USD").put("sell", 28.6f);

        currenciesBank2.put("EUR", new HashMap<>(2, 1.1f));

        currenciesBank2.get("EUR").put("buy", 32.5f);
        currenciesBank2.get("EUR").put("sell", 32.9f);

        currenciesBank2.put("RUB", new HashMap<>(2, 1.1f));

        currenciesBank2.get("RUB").put("buy", 0.54f);
        currenciesBank2.get("RUB").put("sell", 0.53f);

        ///////////////////////////CURRENCY EXCHANGE

        Map<String, Map<String, Float>> currenciesExchange = new HashMap<>();

        currenciesExchange.put("USD", new HashMap<>(2, 1.1f));
        currenciesExchange.get("USD").put("buy", 27.9f);
        currenciesExchange.get("USD").put("sell", 27.8f);

        currenciesExchange.put("EUR", new HashMap<>(2, 1.1f));

        currenciesExchange.get("EUR").put("buy", 31.5f);
        currenciesExchange.get("EUR").put("sell", 31.9f);

        ///////////////////////////CURRENCY EXCHANGE 2

        Map<String, Map<String, Float>> currenciesExchange2 = new HashMap<>();

        currenciesExchange2.put("USD", new HashMap<>(2, 1.1f));
        currenciesExchange2.get("USD").put("buy", 27.1f);
        currenciesExchange2.get("USD").put("sell", 27.2f);

        currenciesExchange2.put("EUR", new HashMap<>(2, 1.1f));

        currenciesExchange2.get("EUR").put("buy", 31.3f);
        currenciesExchange2.get("EUR").put("sell", 31.2f);


        List<Organization> organizations = new ArrayList<>();
        organizations.add(new Bank("privat", "kharkiv", currenciesBank, 12000, 15,
                5,
                1,
                200_000, 25, 12, 18));

        organizations.add(new CurrencyExchange("57kharkiv", "kharkiv", currenciesExchange));
        organizations.add(new Bank("pumb", "Kiev", currenciesBank2, 20000, 15,
                5, 1, 200_000,
                25, 12, 18));
        organizations.add(new CurrencyExchange("38Kiev", "Dnepr", currenciesExchange2));

        organizations.add(new PawnShop("lombard dars", "kharkiv", 50_000, 40));
        organizations.add(new PawnShop("lombard abbars", "kharkiv1", 150_000, 30));
        organizations.add(new PawnShop("lombard abbars2", "kharkiv2", 250_000, 35));

        organizations.add(new CreditCafe("vse chestno pravda", "Kharkiv", 4000f, 200));
        organizations.add(new CreditCafe("vse chestno pravda 2", "Dnepr", 8000f, 250));
        organizations.add(new CreditCafe("vse chestno pravda 3", "Kiev", 6000f, 214));

        organizations.add(new CreditUnit("UnitCredit","Kharkiv", 100_000f, 20));
        organizations.add(new CreditUnit("UnitCredit 2","Kharkiv 2", 120_000f, 23));
        organizations.add(new CreditUnit("UnitCredit 3","Kharkiv 3", 140_000f, 22));

        organizations.add(new InvestmentFund("happy future", "nonoselovka", 17f, 12));
        organizations.add(new InvestmentFund("happy future 2", "nonoselovka 2", 18f, 12));
        organizations.add(new InvestmentFund("happy future 3", "nonoselovka 3", 19f, 12));

        organizations.add(new Post("post 22", "Kiev", 2f));
        organizations.add(new Post("post 24", "Kiev", 3f));
        organizations.add(new Post("post 267", "Kiev", 4f));

        return organizations;
    }
}
