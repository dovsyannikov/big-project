package com.company;

import com.company.model.*;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        List<Organization> organizations = Generator.generate();

        System.out.println("Hello , what kind of operation do you want to execute (exchange/deposit/credit/send money/)");
        Scanner scanner = new Scanner(System.in);
        String operationType = scanner.nextLine();

        switch (operationType.toLowerCase()) {
            case "exchange":
                ExchangingMoney(organizations);
                break;
            case "credit":
                CreditMoney(organizations);
                break;
            case "deposit":
                DepositMoney(organizations);
                break;
            case "send money":
                SendMoney(organizations);
                break;
            default:
                System.out.println("Smth goes wrong");
                break;
        }
    }
    private static void SendMoney(List<Organization> organizations) {
        float amountOfMoney = UserQuestions.getFloatMoneyAmount();
        Organization bestOrganization = Helpers.getOrganizationWithLowestSendinfMoneyComissionProposition(organizations, amountOfMoney);

        System.out.println(String.format("If you want to send %.2f grivnas to someone, he will get %.2f grivnas (this is our best proposition)",
                amountOfMoney,
                ((SendMoney) bestOrganization).sendMoney(amountOfMoney)));
        System.out.println(bestOrganization.toString());
    }

    private static void DepositMoney(List<Organization> organizations) {
        float depositSum = Helpers.getDepositMoneyAmount();
        float periodInMonth = UserQuestions.getPeriodInMonths();

        Organization bestOrganization = Helpers.getOrganizationWithBestDepositProposition(organizations,depositSum,periodInMonth);

        System.out.println(String.format("If you invest %.2f grivnas on period of %.0f months, you will get at the end %.2f",
                depositSum,
                periodInMonth,
                ((Deposit) bestOrganization).deposit(depositSum, periodInMonth)));
        System.out.println(bestOrganization.toString());
    }

    private static void CreditMoney(List<Organization> organizations) {
        float creditSum = UserQuestions.getCreditMoneyAmount();
        float periodInMonth = UserQuestions.getPeriodInMonths();
        Organization bestOrganization = Helpers.getOrganizationWithBestCreditProposition(organizations,creditSum,periodInMonth);

        System.out.println(String.format("So if you want to get %.2f uah for %.0f months, after this period you will need to return %.2f uah",
                creditSum,
                periodInMonth,
                ((Credit) bestOrganization).credit(creditSum, periodInMonth)));

        System.out.println(bestOrganization.toString());
    }

    private static void ExchangingMoney(List<Organization> organizations) {
        String usersCurrency = UserQuestions.getCurrencyThatUserHave();
        float usersSum = UserQuestions.getSumThatUserHave();
        String currencyUserWant = UserQuestions.getCurrencyThatUserWant(usersCurrency);

        //trying to find the organization with best course for our needs
        Organization bestOrganization = Helpers.getBestOrganizationForExchangingMoney(organizations, usersCurrency, currencyUserWant, usersSum);
        System.out.println(String.format("After changing your %.2f %s the best course we can propose you will get %.2f %s",usersSum,
                usersCurrency,
                ((Exchange) bestOrganization).exchange(usersSum, usersCurrency, currencyUserWant),
                currencyUserWant));

        System.out.println(bestOrganization.toString());
    }
}
