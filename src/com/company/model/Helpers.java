package com.company.model;

import com.company.Generator;

import java.util.List;
import java.util.Scanner;

public class Helpers {

    public static Organization getBestOrganizationForExchangingMoney(List<Organization> organizations, String usersCurrency, String currencyUserWant, float usersSum){
        float value;
        float minValue = Float.MIN_VALUE;
        float bestValue = 0f;
        Organization bestOrganization = null;

        for (int i = 0; i < organizations.size(); i++) {
            if (organizations.get(i) instanceof Exchange){
                value =  ((Exchange) organizations.get(i)).exchange(usersSum, usersCurrency, currencyUserWant);
                if (value > minValue && value != 0f) {
                    minValue = value;
                    bestOrganization = organizations.get(i);
                }
            }
        }
        return bestOrganization;
    }
    public static Organization getOrganizationWithBestCreditProposition(List<Organization> organizations, float sumUserWant, float period){
        float value;
        float maxValue = Float.MAX_VALUE;
        Organization bestOrganization = null;
        for (int i = 0; i < organizations.size(); i++) {
            if (organizations.get(i) instanceof Credit){
                value =  ((Credit) organizations.get(i)).credit(sumUserWant, period);
                if (value < maxValue && value != 0f) {
                    maxValue = value;
                    bestOrganization = organizations.get(i);
                }
            }
        }
        return bestOrganization;
    }
    public static float getDepositMoneyAmount() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("How much money do you want to invest?");
        float depositSum = Float.parseFloat(scanner.nextLine());
        return depositSum;
    }
    public static Organization getOrganizationWithBestDepositProposition(List<Organization> organizations, float sumUserWant, float period){
        float value;
        float minValue = Float.MIN_VALUE;
        Organization bestOrganization = null;
        for (int i = 0; i < organizations.size(); i++) {
            if (organizations.get(i) instanceof Deposit){
                value =  ((Deposit) organizations.get(i)).deposit(sumUserWant, period);
                if (value > minValue && value != 0f) {
                    minValue = value;
                    bestOrganization = organizations.get(i);
                }
            }
        }
        return bestOrganization;
    }
    public static Organization getOrganizationWithLowestSendinfMoneyComissionProposition(List<Organization> organizations, float sumUserHave){
        float value;
        float minValue = Float.MIN_VALUE;
        Organization bestOrganization = null;
        for (int i = 0; i < organizations.size(); i++) {
            if (organizations.get(i) instanceof SendMoney){
                value =  ((SendMoney) organizations.get(i)).sendMoney(sumUserHave);
                if (value > minValue && value != 0f) {
                    minValue = value;
                    bestOrganization = organizations.get(i);
                }
            }
        }
        return bestOrganization;
    }
}
