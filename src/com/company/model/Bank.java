package com.company.model;

import java.util.Map;

public class Bank extends Organization implements Exchange, Credit, Deposit, SendMoney {

    private static final String BUY_OPERATION = "buy";
    private static final String SELL_OPERATION = "sell";

    private Map<String, Map<String, Float>> currencies;
    private float exchangeLimitInBank;
    private float commissionRateForExchangeInBankInUah;

    //sending money
    private float commissionRateForSendingMoneyInBankInUah;
    private float commissionRateForSendingMoneyInPercentInBank;

    //credit
    private float creditMaxLimitInBankInUah;
    private float creditProfitPerYeahInPercentInBank;

    //deposit
    private float depositMaxLengthInMonthInBank;
    private float depositProfitAnnualInPercentinBankInUah;

    public Bank(String name, String address, Map<String, Map<String, Float>> currencies,
                float exchangeLimitInBank, float commissionRateForExchangeInBankInUah,
                float commissionRateForSendingMoneyInBankInUah, float commissionRateForSendingMoneyInPercentInBank,
                float creditMaxLimitInBankInUah, float creditProfitPerYeahInPercentInBank,
                float depositMaxLengthInMonthInBank, float depositProfitAnnualInPercentinBankInUah) {
        super(name, address);
        this.currencies = currencies;
        this.exchangeLimitInBank = exchangeLimitInBank;
        this.commissionRateForExchangeInBankInUah = commissionRateForExchangeInBankInUah;
        this.commissionRateForSendingMoneyInBankInUah = commissionRateForSendingMoneyInBankInUah;
        this.commissionRateForSendingMoneyInPercentInBank = commissionRateForSendingMoneyInPercentInBank;
        this.creditMaxLimitInBankInUah = creditMaxLimitInBankInUah;
        this.creditProfitPerYeahInPercentInBank = creditProfitPerYeahInPercentInBank;
        this.depositMaxLengthInMonthInBank = depositMaxLengthInMonthInBank;
        this.depositProfitAnnualInPercentinBankInUah = depositProfitAnnualInPercentinBankInUah;
    }

    public Map<String, Map<String, Float>> getCurrencies() {
        return currencies;
    }

    @Override
    public float exchange(float usersSum, String usersCurrency, String currencyThatUserWant) {
        if (getCurrencyUah().equalsIgnoreCase(usersCurrency) && usersSum < exchangeLimitInBank) {
            return (usersSum - commissionRateForExchangeInBankInUah) / getCurrencies().get(currencyThatUserWant.toUpperCase()).get(BUY_OPERATION);
        } else if (!getCurrencyUah().equalsIgnoreCase(usersCurrency) && usersSum < exchangeLimitInBank){
            return usersSum / getCurrencies().get(usersCurrency.toUpperCase()).get(SELL_OPERATION);
        } else {
            return 0f;
        }
    }

    @Override
    public String toString() {
        String newLine = System.getProperty("line.separator");
        return "Bank{" +
                "currencies=" + currencies +
                newLine +
                ", exchangeLimitInBank=" + exchangeLimitInBank +
                newLine +
                ", commissionRateForExchangeInBankInUah=" + commissionRateForExchangeInBankInUah +
                newLine +
                ", commissionRateForSendingMoneyInBankInUah=" + commissionRateForSendingMoneyInBankInUah +
                newLine +
                ", commissionRateForSendingMoneyInPercentInBank=" + commissionRateForSendingMoneyInPercentInBank +
                newLine +
                ", creditMaxLimitInBankInUah=" + creditMaxLimitInBankInUah +
                newLine +
                ", creditProfitPerYeahInPercentInBank=" + creditProfitPerYeahInPercentInBank +
                newLine +
                ", depositMaxLengthInMonthInBank=" + depositMaxLengthInMonthInBank +
                newLine +
                ", depositProfitAnnualInPercentinBankInUah=" + depositProfitAnnualInPercentinBankInUah +
                newLine +
                ", name='" + name + '\'' +
                newLine +
                ", address='" + address + '\'' +
                '}';
    }

    @Override
    public float credit(float usersSum, float periodInMonth) {
        if (usersSum <= creditMaxLimitInBankInUah) {
            return usersSum + ((usersSum * ((creditProfitPerYeahInPercentInBank / 12) * periodInMonth)) / 100 );
        } else {
            return 0f;
        }
    }

    @Override
    public float deposit(float usersSum, float periodInMonth) {
        if (periodInMonth <= depositMaxLengthInMonthInBank) {
            return usersSum + (usersSum / 100) * ((depositProfitAnnualInPercentinBankInUah / 12) * periodInMonth);
        } else {
            return 0f;
        }
    }

    @Override
    public float sendMoney(float usersSum) {
        return (usersSum - ((usersSum / 100) * commissionRateForSendingMoneyInPercentInBank)) - commissionRateForSendingMoneyInBankInUah;
    }
}
