package com.company.model;

import java.util.Scanner;

public class UserQuestions {
    public static String getCurrencyThatUserHave(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please fill the currency that you want to exchange (USD/UAH/RUB/EUR)");
        String usersCurrency = scanner.nextLine();
        return usersCurrency;
    }
    public static float getSumThatUserHave(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Fill the sum that you have");
        float usersSum = Float.parseFloat(scanner.nextLine());
        return usersSum;
    }
    public static String getCurrencyThatUserWant(String currencyThatUserHave){
        if (currencyThatUserHave.equalsIgnoreCase("uah")){
            System.out.println("Fill the currency that you want");
            Scanner scanner = new Scanner(System.in);
            String currencyThatUserWant = scanner.nextLine();
            return currencyThatUserWant;
        } else {
            System.out.println("We can change your money only to UAH.....");
            return currencyThatUserHave;
        }
    }
    public static float getCreditMoneyAmount() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("How much money do you want to get?");
        float creditSum = Float.parseFloat(scanner.nextLine());
        return creditSum;
    }
    public static float getPeriodInMonths() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("For how much time do you want to get it (in months)?");
        float periodInMonth = Float.parseFloat(scanner.nextLine());
        return periodInMonth;
    }
    public static float getFloatMoneyAmount() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("How much money do you want to send?");
        float sum = Float.parseFloat(scanner.nextLine());
        return sum;
    }
}
