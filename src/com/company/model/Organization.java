package com.company.model;

public abstract class Organization {
    protected String name;
    protected String address;

    //////////////////////////////////////////////////////////////
    private static final String CURRENCY_UAH = "UAH";
    private static final String CURRENCY_EUR = "EUR";
    private static final String CURRENCY_RUB = "RUB";
    private static final String CURRENCY_USD = "USD";

    public static String getCurrencyUah() {
        return CURRENCY_UAH;
    }

    public static String getCurrencyEur() {
        return CURRENCY_EUR;
    }

    public static String getCurrencyRub() {
        return CURRENCY_RUB;
    }

    public static String getCurrencyUsd() {
        return CURRENCY_USD;
    }

    public Organization(String name, String address) {
        this.name = name;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Organization{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
