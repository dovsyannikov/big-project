package com.company.model;

public class Post extends Organization implements SendMoney {
    private float comissionForSendingInPercent;

    public Post(String name, String address, float comissionForSendingInPercent) {
        super(name, address);
        this.comissionForSendingInPercent = comissionForSendingInPercent;
    }

    @Override
    public float sendMoney(float usersSum) {
        return usersSum - ((usersSum / 100) * comissionForSendingInPercent);
    }

    @Override
    public String toString() {
        return "Post{" +
                "comissionForSendingInPercent=" + comissionForSendingInPercent +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }


}
