package com.company.model;

public interface Exchange {
    float exchange(float usersSum, String usersCurrency, String currencyThatUserWant);
}
