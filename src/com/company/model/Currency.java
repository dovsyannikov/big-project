package com.company.model;

public class Currency {
    private String currencyName;
    private float buyCourse;
    private float sellCourse;



    public Currency(String currencyName, float buyCourse, float sellCourse) {
        this.currencyName = currencyName;
        this.buyCourse = buyCourse;
        this.sellCourse = sellCourse;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public float getBuyCourse() {
        return buyCourse;
    }

    public float getSellCourse() {
        return sellCourse;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "currencyName='" + currencyName + '\'' +
                ", buyCourse=" + buyCourse +
                ", sellCourse=" + sellCourse +
                '}';
    }
}
