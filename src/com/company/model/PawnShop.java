package com.company.model;

public class PawnShop extends Organization implements Credit {
    private float creditLimit;
    private float creditPercentPerYear;

    public PawnShop(String name, String address, float creditLimit, float creditPercentPerYear) {
        super(name, address);
        this.creditLimit = creditLimit;
        this.creditPercentPerYear = creditPercentPerYear;
    }

    @Override
    public float credit(float usersSum, float periodInMonth) {
        if (usersSum <= creditLimit) {
            return usersSum + ((usersSum * ((creditPercentPerYear / 12) * periodInMonth)) / 100 );
        } else {
            return 0f;
        }
    }

    @Override
    public String toString() {
        return "PawnShop{" +
                "creditLimit=" + creditLimit +
                ", creditPercentPerYear=" + creditPercentPerYear +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
