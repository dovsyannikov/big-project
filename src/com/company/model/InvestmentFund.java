package com.company.model;

public class InvestmentFund extends Organization implements Deposit {
    private float depositPercentPerYear;
    private float minDepositPeriodInMonth;

    public InvestmentFund(String name, String address, float depositPercentPerYear, float minDepositPeriodInMonth) {
        super(name, address);
        this.depositPercentPerYear = depositPercentPerYear;
        this.minDepositPeriodInMonth = minDepositPeriodInMonth;
    }

    @Override
    public float deposit(float usersSum, float periodInMonth) {
        if (periodInMonth >= minDepositPeriodInMonth) {
            return usersSum + (usersSum / 100) * ((depositPercentPerYear / 12) * periodInMonth);
        } else {
            return 0f;
        }
    }

    @Override
    public String toString() {
        return "InvestmentFund{" +
                "depositPercentPerYear=" + depositPercentPerYear +
                ", minDepositPeriodInMonth=" + minDepositPeriodInMonth +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
