package com.company.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CurrencyExchange extends Organization implements Exchange{

    private static final String BUY_OPERATION = "buy";
    private static final String SELL_OPERATION = "sell";

    private Map<String, Map<String, Float>> currencies;

    public CurrencyExchange(String name, String address, Map<String, Map<String, Float>> currencies) {
        super(name, address);
        this.currencies = currencies;
    }

    public Map<String, Map<String, Float>> getCurrencies() {
        return currencies;
    }

    @Override
    public float exchange(float usersSum, String usersCurrency, String currencyThatUserWant) {
        if (getCurrencyUah().equalsIgnoreCase(usersCurrency)) {
            return usersSum / getCurrencies().get(currencyThatUserWant.toUpperCase()).get(BUY_OPERATION);
        } else {
            return usersSum / getCurrencies().get(usersCurrency.toUpperCase()).get(SELL_OPERATION);
        }
    }


    @Override
    public String toString() {
        String newLine = System.getProperty("line.separator");
        return "CurrencyExchange{" +
                newLine +
                "currencies=" + currencies +
                newLine +
                ", name='" + super.name + '\'' +
                newLine +
                ", address='" + super.address + '\'' +
                '}';
    }
}
