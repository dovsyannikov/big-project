package com.company.model;

public interface Deposit {
    float deposit (float usersSum, float periodInMonth);
}
